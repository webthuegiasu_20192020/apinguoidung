let mongoose = require("mongoose");
let bcrypt = require("bcrypt");

let reviewSchema = new mongoose.Schema({
    idContract: {type: mongoose.Types.ObjectId, require: true},
    rate: Number,
    content: String,
    time: Date
});


module.exports = mongoose.model("Review", reviewSchema);