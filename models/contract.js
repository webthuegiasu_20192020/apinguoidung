let mongoose = require("mongoose");
let bcrypt = require("bcrypt");

let contractSchema = new mongoose.Schema({
    tutor_id: mongoose.Types.ObjectId,
    student_id: mongoose.Types.ObjectId,
    hours: Number,
    skill_id: [mongoose.Types.ObjectId],
    condition: String,
    address: String,
    hourlyrate: Number,
    status: {type:String, default:"Pending"},
    date_send: {type: Date, default: Date.now},
    date_accept: Date,
    date_finish: Date
});


module.exports = mongoose.model("Contract", contractSchema);