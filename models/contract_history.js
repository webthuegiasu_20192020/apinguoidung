let mongoose = require("mongoose");
let bcrypt = require("bcrypt");

let contract_historySchema = new mongoose.Schema({
    idContract: {type: mongoose.Types.ObjectId, require: true},
    state: String,
    time: Date
});


module.exports = mongoose.model("Contract_history", contract_historySchema);