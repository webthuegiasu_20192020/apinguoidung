var express = require('express');
var router = express.Router();
let passport = require("../middleware/passport")
let User= require('../models/user');
let bcrypt = require('bcrypt');
let jwt= require('jsonwebtoken');
let Skill = require('../models/skill');
let Level = require('../models/level');
let Contract = require('../models/contract')

//register function (CHECKED)
router.post('/add', async (req,res)=>{
	User.findOne({ email: req.body.email},async (err,result)=>{
		if(result) return res.send(`This email has been taken`)
	const hashpassword=bcrypt.hashSync(req.body.password,10);
  	const user = new User({
		username: req.body.username,
		password: hashpassword,
		email: req.body.email,
		firstname: req.body.firstname,
		lastname: req.body.lastname,
		country: req.body.country,
		type: 0
	  });
  	try{
		const savedUser = await user.save();
		res.send(savedUser);
  	}catch{
 		res.status(400).send(err);
	  }
	}
)})

//login (CHECKED)
router.post("/login", async(req, res, next) => {
	User.findOne({ email: req.body.email},(err,user)=>{
	if (user) {
		console.log(user.password)
		console.log(bcrypt.hashSync(req.body.password,10))
		console.log(bcrypt.compareSync(req.body.password,user.password))
		if (bcrypt.compareSync(req.body.password,user.password)) {
			let token = jwt.sign(
				{
					exp: Math.floor(Date.now() / 1000) + 12 * 60 * 60,
					sub: user.email
				},
				process.env.JWT_SECRET
			);
			return res.send(token);
		} else {
			return res.status(403).send("Wrong username or password")
		}
	} else {
		return res.status(403).send("Wrong username or password")
	}
  });
});

// get admin list (CHECKED)
router.get('/', (req,res,next)=>{
    User.find({type: 0 , email: {$ne: "root"}}, (err,admin)=>{
        if(err) return res.send(err);
        if(!admin) return res.send("No data");
        else return res.send(admin);
    })
})

//get specific user by id (CHECKED)
router.get('/user/:_id', (req,res,next) => {
    let id= req.params._id;
    console.log(id);
    User.findOne({_id: id}, (err,user) => {
        if(err) return res.send(err)
        if(!user) return res.send('No data')
        else return res.send(user)
    })
})

//get user list (CHECKED)
router.get('/user',(req,res,next)=>{
    User.find({type:1},(err,user)=>{
        if(err) return res.send(err);
        if(!user) return res.send('No data')
        else return res.send(user)
    })
})

//add skill (CHECKED)
router.post('/skill/add', (req,res,next)=>{
    Skill.find({name: req.body.name}, async(err,skill)=>{
        if(err) return res.send(err)
        console.log(skill)
        if(!skill) return res.send('This skill is already existed');
        else{
            let skill = new Skill({
                name: req.body.name
            })
            try{
                const savedSkill = await skill.save();
                res.send(savedSkill);
            }catch{
                res.status(400).send(err);
            }
        }
    })
})

//add level (CHECKED)
router.post('/level/add', (req,res,next)=>{
    Level.find({name: req.body.name}, async(err,level)=>{
        if(!level) return res.send('This skill is already existed');
        else{
            let level = new Level({
                name: req.body.name
            })
            try{
                const savedLevel = await level.save();
                res.send(savedLevel);
            }catch{
                res.status(400).send(err);
            }
        }
    })
})


//detele skill (CHECKED)
router.post('/skill/delete', (req,res,next)=>{
    Skill.findOneAndDelete({_id:req.body.id}, (err)=>{
		if(err) return res.send(err)
		return res.send("Deleted")
    })
})

//UPDATE SKILL(CHECKED)
router.post('/skill/update', (req,res,next)=>{
	Skill.findOneAndUpdate({_id: req.body.id},{name:req.body.name}, (err)=>{
		if(err) return res.send(err)
		return res.send("Updated")
	})
})


//Change status of one user
router.post('/ban', (req,res,next)=>{
    User.findOne({_id: req.id}, (err, user)=>{
        if(err) return res.send(err)
        if(!user) return res.send("Invalid user")
        user.status = "ban";
        return res.send(user.save());
    })
})

router.get('/contract?offsetValue=value&indexStart=value', (req,res,next) =>{
    offsetValue =req.query.offsetValue
    indexStart = req.query.indexStart
    Contract.find((err,contract)=>{
        if(err) return res.send(err)
        if(!contract) return res.send("No Contract")
        else {
            let list = contract.slice(indexStart,indexStart+offsetValue)
            return res.send(list)
        }
    })
})

router.get('/contract/:id', (req,res,next) =>{
    Contract.find({_id: req.params.id},(err,contract)=>{
        if(err) return res.send(err)
        if(!contract) return res.send("No Contract")
        else return res.send(contract)
    })
})

router.post('/contract/delete', (req,res,next)=>{
    Contract.findOneAndDelete({_id:req.body.id}, (err)=>{
		if(err) return res.send(err)
		return res.send("Deleted")
    })
})

module.exports = router;
