var express = require('express');
var router = express.Router();
let passport = require("../middleware/passport")
let User= require('../models/user')
let User_detail = require('../models/user_detail')
let Contract = require('../models/contract')
let Skill = require('../models/skill')
let bcrypt = require('bcrypt')
let jwt= require('jsonwebtoken')

//register function (CHECKED)
router.post('/register', async (req,res)=>{
	User.findOne({ email: req.body.email},async (err,result)=>{
		if(result) return res.send(`This email has been taken`)
	let {password, repassword} = req.body
	/*if(password.trim().length !==0 && password!==repassword)
	  return res.send('Incorrect repassword')*/
	const hashpassword=bcrypt.hashSync(password,10);
	let user;
	user = new User({
		username: req.body.username,
		password: hashpassword,
		email: req.body.email,
		firstname: req.body.firstname,
		lastname: req.body.lastname,
		country: req.body.country,
		type: req.body.type
	});
	
  	try{
		const savedUser = await user.save();
		res.send(savedUser);
  	}catch{
 		res.status(400).send(err);
	  }
	}
)})

//login (CHECKED)
router.post("/login", async(req, res, next) => {
	User.findOne({ email: req.body.email},(err,user)=>{
	if (user) {
		if (bcrypt.compareSync(req.body.password,user.password)) {
			let token = jwt.sign(
				{
					exp: Math.floor(Date.now() / 1000) + 12 * 60 * 60,
					sub: user.email
				},
				process.env.JWT_SECRET
			);
			return res.send({token:token, type: user.type});
		} else {
			return res.status(403).send("Wrong username or password")
		}
	} else {
		return res.status(403).send("Wrong username or password")
	}
  });
});

// change profile (CHECKED)
router.post('/changeprofile', async (req,res)=>{
	const userDetail = {
		description: req.body.description,
		address: req.body.address,
		creditCard: req.body.creditCard,
		tag: req.body.tag,
		price: req.body.price
	}
	User_detail.findOneAndUpdate({idUser: req.body.idUser}, userDetail, async (err,doc)=>{
		if(err)  return res.send(err)
		if(!doc)
			try{
				const detail = new User_detail({idUser: req.body.idUser,...userDetail})
				const save = await detail.save()
				return res.send(save)
			}catch(e){
				return res.status(400).send(e);
			}
		else{
			return res.send(doc)
		}
	}
)})

//router get this user (CHECKED)
router.get('/', (req,res,next)=>{
	User.findOne({_id: req.body.id}, (err, user)=>{
		if (err) return res.send(err)
		if(!user) return res.send("Something wrong")
		else{
			User_detail.findOne({idUser: user._id},{_id:0, idUser:0, __v:0},(err,detail)=>{
				if(err) return res.send(err)
				return res.send({...user.toObject(),...detail.toObject()})
			})
		}
	})
})


//changepassword of the current user
router.post('/changepassword',(req,res,next)=>{
	if(req.body.password !==req.body.repassword)
		return res.status(400).send("Password and repassword didn't match")
	
	else{
		User.findOneAndUpdate({_id: req.body.id},{password: req.body.password}, (err,user)=>{
			if(err) return res.send(err)
			else return res.status(200).send("Change password success")
		})
	}
})

router.post('/changepicture', (req,res,next)=>{
	
})

//CONTRACT FUNCTION

// get contract list
router.get('/contract', (req,res,next)=>{
	/*Contract.find({student_id: req.body.id}, (err,contract)=>{
		if(err) return res.send(err)
		if(!contract) return res.send("No contract")
		else {
			let list = JSON.parse()
		}
	})*/

	Contract.find({student_id: req.body.id})
	.lean()
	.exec((err,contract)=>{
		if(err) return res.send(err)
		contract.map((c) => {
			User.findOne({_id: c.student_id}).then((err,student)=>{
				c['student_name']= student.username
				console.log(c)
			}),
			User.findOne({_id: c.tutor_id}).then((err,tutor)=>{
				c['tutor_name']= tutor.username
				console.log(c)
			})
		});
		res.send(contract)
	})
})

// get specific contract of this student
router.get('/contract/:_id', (req,res,next)=>{
	Contract.find({_id: req.body.id}, (err,contract)=>{
		if(err) return res.send(err)
		if(!contract) return res.send("No contract")
		else return res.send(contract)
 	})
})

router.post('/contract/post/?tutor=:id', (req,res,next)=>{
	Skill.find({name: { $in:req.body.skill}},(err,skill)=>{
		if(err) return res.send(err)
		if(!skill) return res.send("No skill was founded");
		else return skill
	}).then( async(skill)=>{
		if(!skill) return
		console.log(skill)
		skillid= skill.map(s=>s._id)
		let contract = new Contract({
			tutor_id: req.params.id,
			student_id: req.body.student_id,
			skill_id: skillid,
			hours: req.body.hours,
			hourlyrate: req.body.hourlyrate,
			condition: req.body.condition,
			address: req.body.address
		})
		try{
			const saved = await contract.save();
			res.send(saved);
		  }catch{
			 res.status(400).send(err);
		  }
		}
	)
})







//Facebook login
router.get('/account', ensureAuthenticated, function(req, res){
	res.send('account', { user: req.user });
  });

router.get('/auth/facebook', passport.authenticate('facebook',{scope:'email'}));

router.get('/auth/facebook/callback',
	passport.authenticate('facebook', { successRedirect : '/', failureRedirect: '/login' }),
	function(req, res) {
		res.redirect('/');
});

function ensureAuthenticated(req, res, next) {
	if (req.isAuthenticated()) { return next(); }
	res.redirect('/login')
  }

// Google login

module.exports = router;
