var express = require('express');
var router = express.Router();
let Skill = require('../models/skill');


// get skill list (CHECKED)
router.get('/', (req,res,next)=>{
    Skill.find((err,skill)=>{
      if(err) return res.send(err);
      if(!skill) return res.send('No speciality');
      else return res.send(skill);
    })
  })

module.exports = router